
\section{Adaptive Data Collection}
\label{adaptive}
As we can see from the first part of the report, RAPPOR is used to learn which strings are presented in the sampled population and what their corresponding frequencies are with the knowledge of the set of possible values beforehand. However, due to the constructions of the algorithms, it might not work well in some cases.

Imagine such a user case: 
The developer of a mobile APP wants to know among which age group, this APP is the most popular. To gather information for this knowledge, a questionnaire asking about the user's age is sent out to all the users. Then all the users send their randomized age information as a report to the server by use of RAPPOR's encoding algorithm. Finally, when the servers collect all the reports, the RAPPOR decoding process can be carried out, and the frequency information about age will be listed. Then by searching through the top of the list, the developer can easily find out among which age group, this APP is most commonly being used. RAPPOR works well for this specific case, since the honest answers for age will be an integer among [1, 100], which means the size of the candidate string is 100. From the original RAPPOR paper, we can find out that most of its test cases works under the assumption that the size of the candidate string is around 100. In this case, the decoding process of RAPPOR works well and the resulting frequency result is relatively accurate.

However, if the developer of the mobile APP wants to know more accurate information about the user, for example, instead of age, which is an integer, the developer wants to know among which birth time range, this mobile APP is most commonly used. This range might be one hour, for example. 

These queries might happen in cases like: the developer wants to choose a fixed amount of people (K) among all its users by filtering with such a condition: his/her birth time is within one hour, and the total number of the users whose birth date is among this one-hour range is exactly K, and then these K users will be chosen as lucky users who would receive gifts from the mobile APP's developer. We can abstract such queries as a type of query that filters clients with a particular developer-defined query, and developers can freely define the condition.

\begin{figure}[t]
\centerline{\includegraphics[width=11cm]{adapt1.png}}
\caption{Diagram for ordinary RAPPOR data collection}
\label{fig:adapt1}
\end{figure}

Traditional RAPPOR data collection method and decoding algorithms do not work well in such a user case. Imagine that, in such a case, we directly send out a questionnaire as we did in the ``age-only'' case to users: what is your birth time? Upon receiving such a question, users send their randomized birth date information as a report to the server, which contains information about the birth year, birth month, birth date, birth hour and even birth minute. The encoding step still works fine, but the decoding step would fail in this case. The reason is that: the candidate string space is too huge in this case, since birth time contains multiple parts of independent information, which makes the size of the candidate space very huge. We will talk in detail about this in the following section.

However, from the original RAPPOR paper, we can find that when the sample size/the number of reports is not big enough, the upper limit on the strings whose frequency can be learned will be small when the size of candidate string space is huge. Figure below was taken directly from the RAPPOR paper for illustration purpose.

Based on this figure, we can see that when M is huge, which is the size of the candidate string space, the sample size/number of reports need to be large enough so that a certain number of strings' frequency can be learned. Let us take a look at the M = ${10^9}$ case here. In this case, when the sample size is small (less than 500), not even one string can be discovered by use of RAPPOR algorithm. While when M = 10000, around 3 or 4 strings can be discovered. Clearly this result is also impacted by the selection of other security parameters like ${f, p and q}$.  

Going back to the data collection about user's birth time, clearly such queries are different from the tests that were carried in the original RAPPOR paper, which handles queries about the frequency of a particular candidate string. However, this does not mean that RAPPOR does not work anymore for such queries. In order to use the RAPPOR technique to handle such queries, a new data collection method needs to be developed, in our report, we call it adaptive data collection.

We divide adaptive data collection into two cases as follows.

\begin{figure}[t]
\centerline{\includegraphics[width=11cm]{adapt2.png}}
\caption{Maximum number of discoverable strings vs sample size for different candidate string space size M}
\label{fig:adapt2}
\end{figure}

\subsection{Sample-size decreases as adaptive queries go}
\begin{itemize}
  \item User case:
 Servers want to find out which birth time range (within ten minutes) has the highest frequencies among all its users.
Users are asked about their birth time in the form of YEAR.MONTH.DATE.HOUR.MINUTE, from range ${[1900.00.00.00.00-2000.00.00.00.00]}$. 

Here clearly the size of the candidate string is ${100*12*30*24*60=51,840,000}$, which is quite huge.
Suppose initially we have N users in total.
In this case, we can query the users by following the next steps:
 \begin{figure}[t]
\centerline{\includegraphics[width=11cm]{adapt3.png}}
\caption{Diagram for adaptive data collection for RAPPOR}
\label{fig:adapt3}
\end{figure}

  \item Steps developed:
  \begin{itemize}
  	\item Step 1:\\
Among all the N users:\\
Ask about their birth YEAR, in this case, the candidate string is of size 100, when RAPPOR algorithm works well. 
Then find out which year has the highest frequency among all the users, suppose the final result shows that YEAR\_FINAL has the highest frequency, which is f\_year. \\
Construct a new group of clients, which has ${N*f\_year}$ people in it, and all these people reported YEAR\_FINAL in response to the birth time.
  	\item Step2:\\
Among ${N*f\_year}$ clients, a group constructed in step 1.\\
The second query for this new group would be: what is your birth MONTH. Here the candidate string size is 12. Again, RAPPOR algorithm works fine in this case.\\
Then find out which month has the highest frequency among all the users, suppose the final result shows that MONTH\_FINAL has the highest frequency, and the corresponding frequency for MONTH\_FINAL is f\_month. \\
Then A new group of clients are constructed, which has ${(N*f\_year)*f\_month}$ people in it, and all these people in this group reported YEAR\_FINAL and MONTH\_FINAL as part of their birth time.
	\item Step3:\\
Similarly asking the new group of clients about their birth date.
	\item Step4:\\
Similarly for HOUR.
	\item Step5:\\
Similarly for MINITE.
Suppose the corresponding results from step 3, 4 and 5 are: highest frequencies in the corresponding subgroups are: f\_date, f\_hour and f\_minute.
	\item Step6:\\
Final result.\\
It is easy to conclude that the highest frequency for the birth time would then be:\\
f = f\_year*f\_month*f\_date*f\_hour*f\_minute, and the final counts would be: N*f.\\
By this step, we find out that: YEAR\_FINAL, MONTH\_FINAL, DATE\_FINAL, HOUR\_FINAL, MINUTE\_FIANL is the result for the question.
  \end{itemize}
  \item Analyzation:\\
We can see from the above example that, by dividing the big query: which is your birth time (YEAR+MONTH+DATE+HOUR+MINUTE) into five small queries, each time we only need to work on small sample sizes. Such small candidate size spaces make sure that even when the sample size is not huge, the decoding process is still accurate and makes sense. Also, by dividing the queries into smaller queries, we equivalently filter users and make the number of clients smaller as more queries are being conducted. Thus each time, we are only working on a fraction of users' reports, which makes the decoding process easier. Another advantage for dividing queries into several smaller queries is that: each time we only need to store part of the birth time information instead of storing all the birth related information. Therefore, by adopting this adaptive data collection method, we bring less overhead to the storage space.
\end{itemize}


\subsection{Sample-size stays the same as adaptive queries go}
One characteristic of the above case is that the size of the candidate string gets smaller as queries go. However, there are also many cases where all the users? opinions needs to be taken into consideration as queries go.
\begin{itemize}
  \item User case:\\
Suppose now a research group wants to find out what kind of face features are most popular among people. Face features include skin colors, eye colors, nose shapes and etc.
  \item Tests:\\
A face picture is generated by combing the sampled face features. In total, there are M such combinations, which constructs the candidate string space. 
In this case, since there are too many combinations of different face features (M is huge), thus it is not possible to carry out RAPPOR algorithm directly by sending the clients all these N pictures and ask them to report the number/numbers of their favorite face. We already mentioned the reason for not being able to do this in case 1. 
We can instead conduct the experiment in such a way: 
  \item Adaptive query steps:
  \begin{itemize}
  	\item Step 1:\\
First, divide these M faces pictures roughly into two (can be more) categories, based on their most basic face feature, like: relatively white skin and relatively dark skin. And then send the N clients these two sets of pictures, and ask them to pick one set from these two. 
  	\item Step2:\\
Then we choose the set that has more votes compared to the other and discard the other one.\\
Again, we divide the set that was chosen in the first step, and then divide this smaller new set into two/more sets, send them to the clients, and ask them to choose the set that they like more.\\
	...
  \end{itemize}
  \item {Analysis:\\
Such dividing and query process can be carried out again and again until the resulting set is small enough as being able to extract the final favorite face feature combination.
Note that in this case, the size of the sample can be manually adjusted each time by dividing the large set into multiple smaller sets. Or in another word, the developers can freely determine the size of candidate string space for each step. For example, if the original pictures are divided into two sets, then the size of candidate string is two.
 
Different from case 1, in this case, the size of the clients group is always N, since all the users can contribute in all the steps for the final answer. Or equivalently, we can say that within case 2, the query process does not adaptively pick its clients.}  

 
By using the adaptive data collection approach, we can handle the cases when the size of the sample size space M is too huge for RAPPOR to produce accurate results. Also from the above analysis, we can see that by using the adaptive data collection approach as we proposed, the decoding process is more efficient both in terms of decoding time and storage space. 
\end{itemize}

%Our more detailed survey of hardware and software verification methodologies \cite{Demir:2016} has extended details about these, and other, projects.

