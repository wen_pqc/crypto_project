

\section{Parameters Tuning}
\label{params}

In this section, we will show how different parameters would impact the final prediction result.
First, we will show that the selection of different security parameters f, p, and q would 
result in very different analysis precision. Later, we would show that the selection of the number
of clients/reports will also affect the precision of the decoding results, when the number of 
candidate strings is fixed.

\subsection{Impact of f}
In the RAPPOR algorithm, the first randomization step in the encoding part is a permanent randomization which works as follows:

For each client’s value ${v}$, which can be expanded into a (${k}$+1)-bit binary string ${B}$, each bit ${i}$ in ${B}$ will be conditionally flipped, which create a binary reporting value ${B'_i}$, where:

\[  
B'_i = 
\left \{
  \begin{tabular}{ccc}

  1, with probability ${\frac{1}{2}f}$  \\
  0, with probability ${\frac{1}{2}f}$\\
  ${B_i}$, with probability ${1-f}$
  \end{tabular}
\right.
\]

Here we can see that this randomization step is very similar to the “random response”, which behaves as follows:

\[ 
B'_i = 
\left \{
  \begin{tabular}{ccc}
  coin flip, with probability ${\frac{1}{2}}$\\
  ${B_i}$, with probability ${\frac{1}{2}}$
  \end{tabular}
\right.
\]

which can be easily rearranged in this way:
\[ 
B'_i = 
\left \{
  \begin{tabular}{ccc}
  1, with probability ${\frac{1}{4}}$\\
  0, with probability ${\frac{1}{4}}$\\
  ${B_i}$, with probability ${\frac{1}{2}}$
  \end{tabular}
\right.
\]

Here we can see that the first randomization step in RAPPOR is very similar to the “random response” technique, if f = 0.45, then the first randomization step of RAPPOR and ”random response“ are exactly the same.

Formally speaking, we can use a differential parameter ${\epsilon_\infty}$ to measure and define the differential privacy of permanent randomized response, which is\cite{erlingsson2014rappor}:
$$ \epsilon_\infty = 2k*log_e(\frac{1-\frac{1}{2}f}{\frac{1}{2}f}), $$
Where k is the size of bloom filter.

Obviously, this parameter shows the limitation of data's differential privacy, the value of it should be as small as possible when only considering protecting differential privacy. However, on the other hand, when the value of ${\epsilon_\infty}$ becomes smaller, f becomes larger, and more randomization will be introduced so the original true data will be reflected on the RAPPOR result less.

Next let’s see how f would affect the randomization step by looking at the extreme cases. If f = 0, the randomization turns into:

\[
B'_i = B_i, \text{with probability 1}
\]


In this case (f = 0), there is actually no randomization on true data B (${\epsilon_\infty}$ is ${\infty}$), thus f = 0 provides no privacy in terms of true data protection, but since ${B_i'=B_i}$ in this step, decoding will fit the reports very well since it carry most of the information about the original true data.

Another extreme case would be f = 1, which results in the following random flipping:

\[ 
B'_i = 
\left \{
  \begin{tabular}{ccc}
  0, with probability ${\frac{1}{2}}$\\
  1, with probability ${\frac{1}{2}}$
  \end{tabular}
\right.
\]

Clearly this randomization maps to a coin flipping and ${\epsilon_\infty}$ is 0, but at the same time it loses all information about ${B}$, 
Thus we can see that f = 0 provides no randomization, while f = 1 provides too much randomization. When f ranges between [0, 1], the bigger f is, the more randomization would be provided by the permanent randmization step, the less original true data will be reflected in the actual frequency fitting.

We tried to run several tests to prove the above analysis. We sampled a Gaussian distribution with candidate string space size ${M = 100}$, number of clients 
${N = 100000, p = 0.25, q = 0.75, k = 32, h = 1}$, 
number of cohorts = 64, and each client is queried for 10 times. 
In total we collect 1000000 reports.

The above parameters are fixed while we vary f, and here are some tests results:
%\paragraph{f = 0}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{f_0.png}
        \caption{function f = 0, ${\epsilon_\infty}$ = ${\infty}$}
        \label{fig:f_0}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{f_025.png}
        \caption{function f = 0.25, ${\epsilon_\infty}$ = 7.784}
        \label{fig:f_025}
    \end{subfigure}

    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{f_050.png}
        \caption{function f = 0.50, ${\epsilon_\infty}$ = 4.394}
        \label{fig:f_050}
    \end{subfigure}
    %\caption{f}\label{fig:f}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{f_075.png}
        \caption{function f = 0.75, ${\epsilon_\infty}$ = 2.043}
        \label{fig:f_075}

    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{f_095.png}
        \caption{function f = 0.95, ${\epsilon_\infty}$ = 0.400}
        \label{fig:f_095}
    \end{subfigure}
     
    \caption{Impact of f}\label{fig:f}
\end{figure}

 

 

When f = 1, the decoding step fails, which is due to the fact that in this case
the permanent randmization "fully" randomizes the true data and therefore the
reports contain no information about the original true data.

\subsection{Impact of p and q}

Next we would take a look at the impact of the security parameters used on the second randomization step. After the permanent randomization step, ${B'_i}$ will be randomized by another instantaneous randomization step, which works as follows:

\[ 
P(S_i = 1) = 
\left \{
  \begin{tabular}{ccc}
  q, if ${B'_i = 1.}$  \\
  p, if ${B'_i = 0.}$
  \end{tabular}
\right.
\]

We can get:

\begin{equation*}
\begin{split}
P(S_i = 1 | B'_i = 1) = q \\
P(S_i = 1 | B'_i = 0) = p
\end{split}
\end{equation*}
%\[
%P(S_i = 1 | B'_i = 1) = q \\
%P(S_i = 1 | B'_i = 0) = p
%\]

Intuitionally speaking, if ${\frac{P(S_i = 1 | B'_i = 1)}{P(S_i = 1 | B'_i = 0)}}$
is close to 1, then it is a good randomization, since the observation of ${S_i}$ does not reveal any information about Bi’. Therefore, if ${p = q}$, this brings fully randomization, this is one extreme case. But since it is fully random, the decoding simply fails since the reports from the clients carry no information about the original true data.

Another extreme case would be: if ${q = 0, p = 1}$, which is:
\begin{equation*}
\begin{split}
P(S_i = 1 | B'_i = 1) = 0 \\
P(S_i = 1 | B'_i = 0) = 1
\end{split}
\end{equation*}

In this case, we can see that the observation of ${S_i}$ would fully reveal the information of Bi’, which means the selection of ${q = 0, p = 1}$ brings no randomization at all but at the same time, the decoding will fit the reports very well since it carry most of the information about the original true data.

A more general observation would be the value of ${\frac{p}{q}}$, we can use its log version to better reflect the relation between ${p}$ and ${q}$, here we refer the relation to be ${R}$:
$$ R = |log_{2} (|\frac{p}{q}|)| $$
From the previous analysis and property of log function, we can see that bigger ${R}$ means less randomization brought to the instantaneous randomization step, which means it is more difficult for the adversary to find distinguish different ${B_i'}$.

Formally speaking, we can use a differential parameter ${\epsilon_1}$ to measure and define the limitation of the difference between two variables in the dataset\cite{erlingsson2014rappor}, which is:
$$ \epsilon_1 = k*log_e(\frac{q*(1-p*)}{p*(1-q*)}) $$
And ${p*,\; q*}$ represents:
$$ p* = P(S_i=1|b_i=0) = \frac{1}{2}f(p+q) + (1-f)p, \;  q* = P(S_i=1|b_i=1) = \frac{1}{2}f(p+q) + (1-f)q$$
${S_i}$ is a single final report and ${b_i}$ is the value in bloom filter format after step ``Signal''.

We tried to run several tests to prove the above analysis. We sampled a Gaussian distribution with candidate string space size ${M = 100}$, number of clients ${N = 100000, f = 0.45, k = 32, h = 2}$, number of cohorts = 64, and each client is queried for 10 times. 
In total we collect 1000000 reports.

The above parameters are fixed while we vary p and q, and here are some tests results:

Based on these test results, we can see that bigger ${R}$ gives better estimation results, for example, when ${p = 0.25, q = 0}$ and ${R}$ is ${\infty}$, ${\epsilon_1}$ is ${39.735}$, the decoding result matches the theoretical sampling distribution very well but obviously the ${R}$ and ${\epsilon_1}$ is too large. However, when ${R}$ decreases to 1.599 and ${\epsilon_1}$ is 32.693 (${p = 0.25, q = 0.75}$), the decoding result starts to have larger variations compared with the theoretical distribution which can be see from Figure \ref{pq}. When ${R}$ approaches 0 and ${\epsilon_1}$ approaches 0, the decoding simply fails since the reports from the clients carry no information about the original true data because it is fully random and has full differential privacy. This matches our theoretical analysis which means we need to consider both sampling distribution matching and differential privacy. 

\begin{figure}
\label{pq}
    \centering

    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_025_q_0.png}
        \caption{${p = 0.25, q = 0, \frac{p}{q} = \infty}$, ${R}$ = ${\infty}$, ${\epsilon}$ = 39.735} 
        \label{fig:p_025_q_0}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_1_q_050.png}
        \caption{${p = 1.00, q = 0.450, \frac{p}{q} =2.22}$, ${R}$ = 1.152, ${\epsilon}$ = 47.4422}
        \label{fig:p_1_q_050}
    \end{subfigure}
    
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_1_q_075.png}
        \caption{${p = 1.00, q = 0.75, \frac{p}{q} =1.33}$, ${R}$ = 0.415, ${\epsilon}$ = 39.735}
        \label{fig:p_1_q_075}
    \end{subfigure}    
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_025_q_075.png}
        \caption{${p = 0.25, q = 0.75, \frac{p}{q} =0.33}$, ${R}$ = 1.599, ${\epsilon}$ = 32.693}
        \label{fig:p_025_q_075}
    \end{subfigure}   
    
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_090_q_095.png}
        \caption{${p = 0.90, q = 0.95,  \frac{p}{q} = 0.95}$, ${R}$ = 0.074, ${\epsilon}$ = 11.632}
        \label{fig:p_090_q_095}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{p_025_q_020.png}
        \caption{${p = 0.25, q = 0.20, \frac{p}{q} = 1.25}$, ${R}$ = 0.321, ${\epsilon}$ = 4.592}
        \label{fig:p_025_q_020}
    \end{subfigure}
     
    
    \caption{Impact of p and q (second part)}
\end{figure}


\subsection{Impact of N: number of reports}

Another thing we would like to investigate is the relationship between result precision and the selection of M and N, which are the size of the candidate string and the size of the reports that are collected. The intuition behind this is that when the size of the candidate string is fixed, enough reports need to be collected to gather useful frequency information about the candidate strings. Theoretically speaking, the more reports that the severs collect, the more precise frequency information it is able to gather about all the possible candidate strings.

We run a bunch of tests to prove this: \\
${M = 100}$ is fixed, N changes.

\begin{figure}
    \centering 
 \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_2_N_5.png}
        \caption{${N = 100000}$}
        \label{fig:M_2_N_5}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_2_N_6.png}
        \caption{${N = 1000000}$}
        \label{fig:M_2_N_6}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_2_N_7.png}
        \caption{${N = 10000000}$}
        \label{fig:M_2_N_7}
    \end{subfigure}

 \caption{Impact of N (M = 100)}\label{fig:N}
\end{figure}

\begin{figure}
    \centering 
 \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_1_N_6.png}
        \caption{${M=10}$}
        \label{fig:M_1_N_6}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_2_N_6.png}
        \caption{${M=100}$}
        \label{fig:M_2_N_6}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{M_3_N_6.png}
        \caption{${M=1000}$}
        \label{fig:M_3_N_6}
    \end{subfigure}

 \caption{Impact of M (N = 100000)}\label{fig:N}
\end{figure}

 
 
 

 

From these figures, we can see that when the size of the candidate string M is fixed, more reports generate more precise frequency analysis result.

Conversely, we can fix the number of the reports ${N = 100000}$, and vary M, which should produce similar results:

Similar patterns can be seem from the above figures as well.























