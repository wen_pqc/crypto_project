
\section{Counting Bloom Filter}
\label{Counting_bf}

\subsection{Multiple-time collection in Basic RAPPOR}

As we mentioned before, the first step in the encoding step is to hash a client's 
value ${v}$ onto the Bloom filter B of size ${k}$ using ${h}$ hash functions. 
If we only focus on the Basic RAPPOR model, then ${h = 1}$, which means each string
${v}$  can be deterministically mapped to a single bit in the bit array by use of
a single hash function.

The design of RAPPOR considers about multiple-time collections, enforced by each
client, which brings the requirement of longitudinal privacy protection. 
Now let us suppose we use a Basic RAPPOR model which has N clients, and each client
is quried 10 times. 
If we look at client Alice only, who is queried 10 times as well. The data collection
works as follows:
\begin{itemize}
\item First, Alice provides her true value ${v_i}$, use the hash function ${h(x)}$ to hash it, and get result ${loc_i}$, set the bit in ${loc_i}$ as 1. Suppose the resulting string 
is noted as B.

\item Then use permanent randomization to randomize ${B_i}$, and get ${B'_i}$.

\item Finally use instantaneous randomization to randomize ${B'_i}$ and get ${S_i}$, which is sent 
\end{itemize}
as the ${i_{th}}$ report.

Alice repeats the process for ${i = 1, 2, ..., 10}$ since she is queried 10 times.
Now let us take a look at the size of reports sent from Alice that the server needs
to store for the final decoding. One single report S takes up ${k}$ bits, then 10 reports
will take ${10 \times k}$ bits. More generally speaking, if Alice is queried W times, then
${W \times k}$ bits need to be stored, which grows linearly with the number of queries that
each client is enforced.

In our work, we extended the Basic RAPPOR model by using the idea of Counting Bloom filter, which targets at saving space for the server when each client is queried multiple times.

\subsection{Extension: Extended Basic RAPPOR with Counting Bloom filter}

\paragraph{Counting Bloom filter}
First of all, our work is extended from the Basic RAPPOR model.
We only focus on a very simple design of Counting Bloom filter, which only has one hash function, and maps the true value ${v}$ to one single bit location ${loc}$ in B.
Next let us take a look at how Counting Bloom filters \cite{nyang2013counting} has counting function.
\begin{itemize}
\item When the first value ${v_1}$ comes, hash it to get a value ${loc_1}$, set the value in ${loc_1}$ as 1.

\item When the ${i_{th}}$ value ${v_i}$ comes, hash it to get a value ${loc_i}$, add the value in ${loc_i}$ by 1.

\item Keep repeating the above step for all ${i = 2, .., W}$
\end{itemize}

From the above description, it is easy to understand why Counting Bloom filter has
counting function.

\paragraph{Basic RAPPOR with Counting Bloom filter}

Now we will show how to use a Counting Bloom filter in the Basic RAPPOR model. First, 
we will describe the data collection/encoding process:

\begin{itemize}

\item \text{Signal.} Hash client's value ${v_i}$ onto the Counting Bloom filter B, which has ${k}$ locations and one single hash function.
Repeat this process for all ${i = 1, 2, .., W}$, note the final result in the Counting Bloom filter as ${C}$.

\item \text{Permanent randomized response.} Given ${C}$, note that in ${C}$, instead of storing a binary bit in each position, an integer number ${C_i}$ is stored, which is between ${[0, W]}$. For each value ${C_i}$ in location i, create ${C'_i}$ which equals to:

\[  
C'_i = 
\left \{
  \begin{tabular}{ccc}

  0, with probability ${\frac{1}{W+1}f}$  \\
  1, with probability ${\frac{1}{W+1}f}$\\
  ... \\
  W, with probability ${\frac{1}{W+1}f}$\\
  ${C_i}$, with probability ${1-f}$
  \end{tabular}
\right.
\]

\item \text{Report.} Set ${S = C'_i}$, send S to the server.
\end{itemize}

Note here that, since we use a Counting Bloom filter to count multiple true values
from each client, only one report per client will be sent to the server. Thus the instantaneous randomization is no longer needed in our Extended Basic RAPPOR model which incorporates a Counting Bloom filter for multiple-time data collection/signalling.

In the next section, we will prove that such a construction satisfies differential privacy.

\subsection{Differential privacy of Extended Basic RAPPOR}

\paragraph{Assumptions}
We already introduced the idea of being differentially private in previous sections, thus we will not repeat this part here.  
In our proof, we assume that ${W = 2}$, which means each client is queried 2 times.%, and we assume that the multiple-time query tries to simulate a multi-choice query.
%We only make this assumption for clarification purpose.
%Under our assumptions, it is easy to see that, for the final result in Counting Bloom
%filter in ${C}$, each location is either 0 or 1, depending on whether the corresponding true value ${v}$ is one of the client's multiple-choices or not.

\paragraph{Theorem.}

 The extended Basic RAPPOR algorithm satisfies ${\epsilon_{\infty}}$-differential privacy where ${\epsilon_{\infty} = 4\text{In}(\frac{1-\frac{2}{3}f}{\frac{1}{3}f})}$ when ${W = 2}$.

\paragraph{Proof.}
Under such assumptions, the permanent randomization step works as follows:
\[  
C'_i = 
\left \{
  \begin{tabular}{ccc}

  0, with probability ${\frac{1}{3}f}$  \\
  1, with probability ${\frac{1}{3}f}$\\
  2, with probability ${\frac{1}{3}f}$\\
  ${C_i}$, with probability ${1-f}$
  \end{tabular}
\right.
\]

Let ${S = s_1, ..., s_k}$ be a randomized report generated by the Extended 
Basic RAPPOR algorithm. Then the probability of observing any given report ${S}$
given the true client value ${V = v = (v_1, v_2)}$ (here ${v_1 \text{and} v_2}$ are the client's reponses for the two queries) is:

\begin{equation*}
\begin{split}
P(S=s|V=v) &= P(S=s|C, C', v) \cdot P(C'|C, v) \cdot P(C|v) \\
&= P(S=s|C') \cdot P(C'|C) \cdot P(C|v) \\
&= P(S=s|C') \cdot P(C'|C) \\
&= P(C'|C)
\end{split}
\end{equation*}

Without loss of generality, we assume that in ${C_1}$, the first 2 bits are set, 

i.e., 
${C_1 = \{c_1 = 1, c_2 = 1, c_3 = 0, ..., c_k = 0\}}$, 

and in in ${C_2}$, bits 3 and 4 are set, i.e., 
${C_1 = \{c_1 = 0, c_2 = 0, c_3 = 1, c_4 = 1, c_5 = 0, ..., c_k = 0\}}$.

Relevant conditional probabilities are:
\begin{equation*}
\begin{split}
&P(c'_i = 0 | c_i = 1) = \frac{1}{3}f \\
&P(c'_i = 1 | c_i = 1) = 1- \frac{2}{3}f \\
&P(c'_i = 2 | c_i = 1) = \frac{1}{3}f
\end{split}
\end{equation*}

and,

\begin{equation*}
\begin{split}
&P(c'_i = 0 | c_i = 0) = 1- \frac{2}{3}f \\
&P(c'_i = 1 | c_i = 0) = \frac{1}{3}f\\
&P(c'_i = 2 | c_i = 0) = \frac{1}{3}f
\end{split}
\end{equation*}

Here we define a Boolean function B:

\[  
B(X = x) = 
\left \{
  \begin{tabular}{ccc}
  0, if X = x  \\
  1, otherwise
  \end{tabular}
\right.
\]

Then, we can calculate ${P(C' = C'| C = C_1) \text{and} P(C' = c'| C = C_2)}$:
\begin{equation*}
\begin{split}
P(C' = c'| C = C_1) = &(1- \frac{2}{3}f)^{B(c'_1 = 1)} \times (\frac{1}{3}f)^{1-B(c'_1 = 1)} \times \\
&(1- \frac{2}{3}f)^{B(c'_2 = 1)} \times (\frac{1}{3}f)^{1-B(c'_2 = 1)} \times \\
&(1- \frac{2}{3}f)^{B(c'_3 = 0)} \times (\frac{1}{3}f)^{1-B(c'_3 = 0)} \times ... \\
&(1- \frac{2}{3}f)^{B(c'_k = 0)} \times (\frac{1}{3}f)^{1-B(c'_k = 0)}
\end{split}
\end{equation*}

Similarly,

\begin{equation*}
\begin{split}
P(C' = c'| C = C_2) = &(1- \frac{2}{3}f)^{B(c'_1 = 0)} \times (\frac{1}{3}f)^{1-B(c'_1 = 0)} \times \\
&(1- \frac{2}{3}f)^{B(c'_2 = 0)} \times (\frac{1}{3}f)^{1-B(c'_2 = 0)} \times \\
&(1- \frac{2}{3}f)^{B(c'_3 = 1)} \times (\frac{1}{3}f)^{1-B(c'_3 = 1)} \times \\
&(1- \frac{2}{3}f)^{B(c'_4 = 1)} \times (\frac{1}{3}f)^{1-B(c'_4 = 1)} \times \\
&(1- \frac{2}{3}f)^{B(c'_5 = 0)} \times (\frac{1}{3}f)^{1-B(c'_5 = 0)} \times ...\\
&(1- \frac{2}{3}f)^{B(c'_k = 0)} \times (\frac{1}{3}f)^{1-B(c'_k = 0)}
\end{split}
\end{equation*}

Let ${RR_{\infty}}$ be the ratio of two such conditional probabilities with distinct values of ${C, C_1 \text{ and } C_2}$, i.e., ${RR_{\infty} = \frac{P(C'\in R^*|C = C_1)}{P(C'\in R^*|C = C_2)}}$. For the differential privacy condition to hold, ${RR_{\infty}}$ needs to be bounded by $\text{exp}(\epsilon_{\infty})$.

\begin{equation*}
\begin{split}
RR_{\infty} & = \frac{P(C'\in R^*|C = C_1)}{P(C'\in R^*|C = C_2)} \\
& = \frac{\sum_{C'_i \in R^*}P(C'=C'_i|C=C_1)}{\sum_{C'_i \in R^*}P(C'=C'_i|C=C_2)} \\
& \leq max_{C'\in R^*} \frac{P(C'=C'_i|C=C_1)}{P(C'=C'_i|C=C_2)} \\
& = (\frac{1}{3}f)^{(B(c'_1=0)-B(c'_1=1)+B(c'_2=0)-B(c'_2=1))-(B(c'_3=0)-B(c'_3=1)+B(c'_4=0)-B(c'_4=1))}\\
& \times (1-\frac{2}{3}f)^{(B(c'_3=0)-B(c'_3=1)+B(c'_4=0)-B(c'_4=1))-(B(c'_1=0)-B(c'_1=1)+B(c'_2=0)-B(c'_2=1))}
\end{split}
\end{equation*}

Sensitivity is maximized when ${B(c'_3=0) = B(c'_4=0) = B(c'_1=1) = B(c'_2=1)}$.

In this case, ${RR_{\infty} = (\frac{1-\frac{2}{3}f}{\frac{1}{3}f})^4}$.

Therefore, ${\epsilon_{\infty} = 4\text{In}(\frac{1-\frac{2}{3}f}{\frac{1}{3}f})}$.

\paragraph{General conclusion}
The above proof assumes that ${W = 2}$, for other possible values of ${W}$, a similar
proof can be carried out without loss of generality.
It is easy to prove the following theorem: if ${W = k}$, then our Extended Basic RAPPOR model satisfies ${\epsilon_{\infty}}$-differential privacy where ${\epsilon_{\infty} = 2k \cdot \text{In}(\frac{1-\frac{k}{k+1}f}{\frac{1}{k+1}f})}$. 

Since the use of Counting Bloom filter gets rid of the requirement of instantaneous randomization, so far, we have finished the proof of the differential privacy of our Extended Basic RAPPOR algorithm.



\subsection{Decoding of extended Basic RAPPOR}
We just proved that our extended Basic RAPPOR algorithm provides differential privacy, now let us take a look at the decoding process in our extended Basic RAPPOR algorithm.

Same as original RAPPOR, the goal of data collection is to learn which strings are present in the sampled population and what there corresponding frequencies are. The decoding process can be carried out as follows(here we are still using ${W = 2}$ for illustration):

We have:

\begin{equation*}
\begin{split}
&P(c'_i = 0 | c_i = 0) = 1- \frac{2}{3}f \\
&P(c'_i = 1 | c_i = 0) = \frac{1}{3}f\\
&P(c'_i = 2 | c_i = 0) = \frac{1}{3}f
\end{split}
\end{equation*}

\begin{equation*}
\begin{split}
&P(c'_i = 0 | c_i = 1) = \frac{1}{3}f \\
&P(c'_i = 1 | c_i = 1) = 1- \frac{2}{3}f\\
&P(c'_i = 2 | c_i = 1) = \frac{1}{3}f
\end{split}
\end{equation*}

\begin{equation*}
\begin{split}
&P(c'_i = 0 | c_i = 2) = \frac{1}{3}f \\
&P(c'_i = 1 | c_i = 2) = \frac{1}{3}f \\
&P(c'_i = 2 | c_i = 2) = 1- \frac{2}{3}f
\end{split}
\end{equation*}

Suppose ${N}$ reports are collected in total, 
let us look at one single value of ${C_i}$. 
Suppose that within ${N}$ reports, ${N'_0}$ of them have ${C'_i = 0}$,
${N'_1}$ of them have ${C'_i = 1}$, and ${N'_2}$ of them have ${C'_i = 2}$.
Here ${N'_0, N'_1 \text{ and } N'_2}$ can be collected by checking the collected
reports. 

Let us assume that in the original true values, ${N_0}$ of them have ${C_i = 0}$,
${N_1}$ of them have ${C_i = 1}$, and ${N_2}$ of them have ${C_i = 2}$.
Then we know that:
\begin{equation*}
\begin{split}
&N_0 \cdot (1- \frac{2}{3}f) + N_1 \cdot \frac{1}{3}f + N_2 \cdot \frac{1}{3}f = N'_0 \\
&N_0 \cdot \frac{1}{3}f + N_1 \cdot (1- \frac{2}{3}f) + N_2 \cdot \frac{1}{3}f = N'_1 \\
&N_0 \cdot \frac{1}{3}f + N_1 \cdot \frac{1}{3}f + N_2 \cdot (1- \frac{2}{3}f) = N'_2 
\end{split}
\end{equation*}

\begin{itemize}
\item By solving this linear equations set, we can easily estimate the values of
${N_0, N_1 \text{ and } N_2}$. Then we can estimate that the total counts for 
position ${i}$ in ${C_i}$ is ${(0\cdot N_0 + 1 \cdot N_1 + 2 \cdot N_2)}$,
let us note these counts as ${t_i}$.

Let Y be a vector of ${t_i, i = 1, .., k}$, i.e., ${Y = t_1, ..., t_k}$.

\item Then we create a design matrix ${X}$ of size ${k \times M}$ where ${M}$ is the 
number of candidate strings under consideration. ${X}$ is mostly 0(sparse)
with 1's at the hashed bit locations for each string. So each column of
${X}$ contains 1 bit 1 at the position where a particular candiate string is mapped
to by the hash function. 

\item Use a linear regression model to fit ${Y \sim X}$.

\item The counts of the candiate strings correpond to the coefficients of the linear
model's coefficients.

The linear regression steps in our Extended Basic RAPPOR model is very similar to the original RAPPOR decoding steps, so we will not talk about it in detail here.
\end{itemize}



\subsection{Advantages of Extended Basic RAPPOR algorithm}
In this section, we will talk about the motivation for using the Extended
Basic RAPPOR algorithm.

First of all, by using a Counting Bloom filter, we do not need the instantaneous
randomization step, which simplifies the encoding/decoding processes.

More importantly, by using our extended Basic RAPPOR algorithm, we can save the 
space in the server that is needed for storing all the reports. Next we'll compare
the usage of server space needed in the original Basic RAPPOR and our Extended Basic
RAPPOR algorithm. Suppose in total, each client is queried ${W}$ times, and the length of the true value ${v}$ is ${k}$. 

In the original Basic RAPPOR model, each query generates a length ${k}$ report, 
where each position has a binary value (0 or 1). Thus in total, after ${W}$ queries,
${W}$ reports are generated and therefore ${k \times W}$ bits are needed for reports
storage.

In our extended Basic RAPPOR model, since we are using a Counting Bloom filter, 
in each position of ${C}$, instead of storing a binary bit, we are string the 
counting result, which ranges between ${[0, W]}$, thus for each position, ${log(W+1)}$-bits are needed. Finally, the randomization step generates a report of size
${k \times log(W+1)}$, which means ${k \times log(W+1)}$-bits are needed for reports storage in our case.

By comparison, we can see that our extended Basic RAPPOR model require only 
${\frac{log(W+1)}{W}}$ portion of the space needed by the original Basic RAPPOR 
algorithm. When ${W = 15}$, our extended Basic RAPPOR model only takes around
73 \% less storage space compared to the original Basic RAPPOR algorithm.






































 





