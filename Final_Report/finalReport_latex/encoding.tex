
\section{RAPPOR Encoding Algorithm and Attack Model}
\label{encoding}

The goal of RAPPOR is to learn which strings are presented in the sampled population and what their corresponding frequencies are with the knowledge of the set of possible values beforehand. 

\subsection{Techniques used in RAPPOR}
\subsubsection{Randomized response}
\begin{itemize}
  \item to collect population-level statistics without access to individual-level data points.
  \item to satisfy a privacy guarantee known as local differential privacy.
\end{itemize}

\subsubsection{Local differential privacy}
\begin{itemize}
  \item Aim: guarantee that analyzer's knowledge about single client's ground truth does not change much based on the information client sends.
  \item Formally, a randomized algorithm ${A}$ satisfies ${\epsilon}$ -differential privacy if for all pairs of client's values ${x_1}$ and ${x_2}$ and for all ${R \in Range(A)}$,  
  $${P(A(x)\in R) \le e^\epsilon P(A(x')\in R), \; for \; all \; R \subseteq Range(A)}$$ \cite{dwork2008differential}.
\end{itemize}

\subsubsection{Bloom filters}
\begin{itemize}
  \item Aim 
  	\begin{itemize} 
		\item Add uncertainty, serve to make reports compact and complicate attacker's decoding difficulty.
		\item Map data of arbitrary size to some fixed size bits.
		\item Save space in the algorithm.
	\end{itemize}
  \item Formally, a hash area is considered as ${N}$  individual addressable bits ${l_i}$ with bit address ${a_i, \; i \in [0, \; N-1]}$
  	\begin{itemize} 
		\item Add bits ${l_i}$, ${i \in [0, N-1]}$ are set to 0.
		\item Message in the set to be stored is separately hash coded by ${d}$ hash functions, say ${h_1, \; h_2 \; ..., \; h_d}$ into a number of distinct bits, say  ${a_1, \; a_2 \; ..., \; a_d}$.
		\item All ${d}$ bits addressed by ${a_1}$ through ${a_d}$ are set to 1.
	\end{itemize}
\end{itemize}



\subsubsection{Parameters}
\begin{itemize}
  \item ${m}$: number of cohorts.
  \item ${N}$: number of clients.
  \item ${v}$: number of queries each client receive.
  \item ${k}$: size of Bloom Filter ${B}$.
  \item ${h}$: hash function number.
  \item ${f}$: security parameter used in the first randomization process.
  \item ${p, q}$: security parameter used in the second randomization process.
   
\end{itemize}

 

\subsection{RAPPOR encoding algorithm}

\subsubsection{Input \& Output}
 
  \paragraph{Input:} any single data collected from each client, could be any string or number ${i \in Z}$, given as value ${v}$, while the candidate string space should be finite.
  \paragraph{Output:} a bit array ${S}$ of size ${k}$; cohort number ${j \in [1, \; m]}$, ${m}$ is the number of cohorts (this number will be used in the decoding process).
    \begin{itemize} 
    \item Reason to set ${m}$ cohorts: for each client's value ${v}$, we could assign a different cohort to it which provides different sets of ${h}$ hash functions for the Bloom Filters. It can significantly decrease the possibilities of accidental collisions of two strings across all of them. Redundancy of running ${m}$ cohorts simultaneously can also greatly improve the false positive rate.
   \end{itemize}
  To sum up, we have the input \& output RAPPOR form: ${v \to S(|S|=k), \; j}$
 
The encoding algorithm composes of three four steps: signal, permanent randomized response, instantaneous randomized response, and report.
 
  Before any data collection begins, each client is randomly assigned and becomes a permanent member of one of  ${m}$ cohorts ${j}$.
  \begin{itemize} 
  \item Signal: hash client's value ${v}$ onto the Bloom Filter ${B}$, output the array ${B}$ with size ${k}$  using ${h}$ hash functions.
  \item Permanent randomized response:
  	\begin{itemize} 
		\item Aim: ensure privacy because adversary has limited ability to differentiate between true and ``noisy'' random 				numbers since ${B'}$ may or may not contain any information about ${B}$.
		\item For each client's value ${v}$ and bit ${i, \; 0 \le i \le k \; \text{in} \; B}$ in , creating a binary reporting value ${B_i'}$, 				where:
		\[  
			B'_i = 
			\left \{
  			\begin{tabular}{ccc}
  			1, with probability ${\frac{1}{2}f}$  \\
  			0, with probability ${\frac{1}{2}f}$\\
  			${B_i}$, with probability ${1-f}$
  			\end{tabular}
			\right.
		\]
		\begin{itemize}
  			\item  ${f}$ controls the noise quantity of this Bloom Filter ${B'}$ and ${f \in [0,\; 1]}$. One way to have an intuition about the effect of parameter f is to take a look at the extreme cases. When ${f=0}$, there is no noise added in this randomization step, thus this step provides no privacy; when ${f=1}$, the above formula is equivalent to a fair coin flipping, which means this step loses all the information about the original data. One special case is when ${f = \frac{1}{2}}$, in this case, this randomization step maps to a randomized response.
  			
		\end{itemize}

    \item ${B'}$ is memorized and reused as the basis for all future reports on this distinct value ${v}$.

		\item Attack model one: The first attacker has access to one single report ${S}$ from a targeted user and is limited 			by one-time differential privacy level ${\epsilon_1}$ on how much knowledge gain is possible. This kind of 			attacker could be prevented by using Permanent randomized response step since we add noise to the original 			Bloom Filter's result.
	\end{itemize}
	
  \item Instantaneous randomized response:
  	\begin{itemize} 
		\item Aim:
		\begin{itemize}
  			%\item  Report a randomized version of ${B'}$.
  			\item In longitudinal reporting, if adversary requests the same client's same value more than once, the output will be an unique identifier which could be taken advantage of by the adversary.
				Longitudinal privacy is used for preventing multiple data collections of the same client from being taken 						advantage of by the adversary.
				\end{itemize}
			\item Provide stronger short-term privacy guarantees (because we add more noise to the report). It then can 				be independently tuned to balance short-term and long-term risk and further balance utility against 					different attacker models (more flexible). 
		\end{itemize}
		
		\item Generate a bit array ${S}$ of size ${k}$ and initialize it as 0. The probability of each bit ${S_i}$ to be 1 is that: for each 			${B_i'}$  derived from the ``noisy'' Bloom Filter which inherited from last step, the probability value of ${S_i}$ to 			be 1 is highly related to the value of ${B_i'}$:
			\[ 
				P(S_i = 1) = 
				\left \{
  				\begin{tabular}{ccc}
  				q, if ${B'_i = 1.}$  \\
  				p, if ${B'_i = 0.}$
 				 \end{tabular}
				\right.
			\]
		\begin{itemize}
  			\item  ${|p-q|}$: better privacy is guaranteed when ${p}$ and ${q}$ are close to each other. However, at the same time, smaller ${|p-q|}$ brings less analysis accuracy.  
		\end{itemize}

		\item Attack model two: The second attacker have access to one client's multiple responses over a well-defined period of time. 			This kind of attack could be prevented by Instantaneous randomized response step. As we illustrated above, 			this step could guarantee the longitudinal differential privacy.
	 
  \item Report: Send the bit array ${S}$ and assigned cohort number ${j}$ to the server.
\end{itemize}

\subsubsection{Visualized RAPPOR encoding model diagram}
\begin{figure}[h]
\centerline{\includegraphics[width=12cm]{encoding.png}}
\caption{Diagram of the RAPPORT encoding process}
\label{fig:encoding}
\end{figure}

\subsubsection{Different kinds of RAPPOR modifications}
\begin{itemize}
  \item One-time RAPPOR: if there exist no multiple data collections from the same user, instantaneous randomized response step could be skipped and it's enough to provide strong privacy protection.
  \item Basic RAPPOR: if client's value ${v}$ is relatively small and well-defined, Bloom Filter step could be skipped. E.g. in our extended work, we will talk about the influence of the size of candidate string space ${M(v \subseteq M)}$ on the analysis accuracy and privacy. For example, if the answer of the client is either female ``0'') or male (``1''), we could simply use a deterministic mapping rather than use Bloom Filter to deal with the raw data.
  \item Basic One-time RAPPOR: this is the most simplified version of RAPPOR algorithm. It neither needs Instantaneous randomized response step nor needs Bloom Filter to deal with raw client value. Just one round of randomization using a deterministic mapping of strings into their own unique bits is needed.
\end{itemize}


%tion will enforce the information flow policy in the resource sharing situation with the dynamic labels.

%\subsection{Dynamic Label Specification}

% interference tables (interference table is discussed in Section \ref{decoding}).

%{\tt val a = UInt (INPUT, 1, "L"); -- for static label}

%{\tt val b = UInt (INPUT, 1, "SecMap(a)"); -- for dynamic label based on value of variable a in current module}
