\section{Experiment and Evaluation of Different Distributions}
\label{distributions}

We want to figure out how well RAPPOR can be applied to different types of distributions. Comparing the analysis accuracy of different distributions not only allows us to see if RAPPOR can be generally applied on different distribution models (The original paper didn't mention this part), but also helps us find a better/the best distribution model to use if we want to achieve higher differential privacy/analysis accuracy by using RAPPOR.

The basic idea is that we first sample a set of true values by following a specific distribution function. Then we choose parameters  and use RAPPOR algorithm to encode these true values. After collecting all the randomized data, which are the outputs of the RAPPOR encoding step, we apply the RAPPOR decoding algorithm to extract the Counting/frequency information.
Then we plot the sampled distribution and the analyzed frequency/counts distribution in the same graph, and visualize how well the analysis result fits the sampled/theoretical distribution.
The higher the analysis accuracy, the better these two distribution diagrams should match each other.

For testing purpose, we use Basic RAPPOR in our experiments, which means there is only one hash function in the Bloom filter.

\subsection{General parameters and judgements used}
In order to make sure that frequency analysis is meaningful, we should make sure we gather enough number of raw data compared with the number of total possible unique true values, which means ${N}$ should be relatively much larger compared to ${M}$. Secondly, the cohort number ${m}$ should be carefully picked to avoid hash collisions. The detailed analysis about parameters tuning can be found in chapter \ref{decoding}. 

In this section, we are targetting at having an visuzlization of how RAPPOR can be applied on different types of distributions rather than finding out the best parameter sets for different distributions. For clarification purpose, we use the same set of parameters for all the distribution testings. 
We will use the following parameters during testing:

\begin{table}[h]
\centering
\small
\caption{Parameters Used in Different Distribution Models}
\vspace{0.1cm}
\label{params}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline
\textbf{parameters}  & \textbf{u} & \textbf{c} & \textbf{v} & \textbf{k} & \textbf{h} & \textbf{m} & \textbf{f} & \textbf{p} & \textbf{q}\\ \hline
\textbf{value} & 100          &100000      & 10         & 32         & 1         & 64         & 0.25         & 0.75        & 0.5          \\ \hline
\end{tabular}
\end{table}

These privacy constraints separately mean:

\begin{itemize}
  \item ${u}$: total unique values. (Except in poisson distribution, u is 71 and in Weibull distribution, u is 14)
  \item ${c}$: client numbers.
  \item ${v}$: number of values per client.
  \item ${k}$: report bits number.
  \item ${h}$: hash function number.
  \item ${m}$: cohort number.
  \item ${f}$: security parameter used in the first randomization process.
  \item ${p, q}$: security parameter used in the second randomization process.
 % \item ${q}$: probability used in the second randomization process.\\
\end{itemize}

We use a few standards to judge if an RAPPOR analysis result fits the theoretical distribution model well or not. A table showing all these information for different distributions is shown at end of this chapter.
\begin{itemize}
  \item r/a: detection rate, number of values RAPPOR detected/number of actual values.
  \item fp: false positive rate, means the true counts are truly 0 but estimated to be significantly different.

  Theoretically speaking, the decoding results should not contain strings that sit outside the range the sampled candiate strings, but the result of RAPPOR does detect some non-candiate strings. It is illustrated in the original RAPPOR paper that: "Notice the noise in the left
and right tails where there is essentially no signal. It is required
by the differential privacy condition and also gives a
sense of how uncertain our estimated counts are." 
We think the reason might be that there exist some decoding errors/faults, which would procude such fault prediction of non-candiate strings. How to avoid such decoding faults might be an interesting topic to investigate in the future. 
  \item tv: total variation distance, counted by the sum of frequency differences for each candidate true value and then doing some uniform processing
  \end{itemize}


\subsection{Different Distribution Models}

\subsubsection{Uniform Distribution}
\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/unif.png}}
\caption{Uniform Distribution Model}
\label{fig:unif}
\end{figure}

The first test that we analyzed is uniform distribution, which means all the candiate strings are even distributed. For example, when throwing a fair dice, the probability of each possible value 1, 2, 3, 4, 5, 6 should be 1/6. We simulated an uniform distributed model with candiate strings ranging between 1 and 100. Result of RAPPOR decoding is shown in figure \ref{fig:unif}. Note that there are noises in the right tails which doesn't belong to possible candidate true value. %So in the differential privacy condition it is needed and it also indicates uncertainty of the estimated counts.

It can be observed from the figure that the fluctuation of frequency for different candidate values can not be ignored, so the variation distance is big, up to 0.160. But the detection rate and false positive rate are great (97/100 and 8.0\%(8), respectively), which means RAPPOR algorithm is sensitive to uniform distribution.

\subsubsection{Gaussian Distribution}

Gaussian distribution model is a commonly used probability distribution. It is an important model in statistics and is often used in the natural and social sciences to represent real-valued random variables when its distributions are unknown yet. Gaussian distribution fitting result is shown in figure \ref{fig:Gaussian}. We sampled the candidate strings([1, 100]) with mean value = 50 and standard deviation value = 100/6. There are also uncertainty of the estimated counts shown by the noice of the right tails (the left tails didn't show up in the graph but they should also exist). 

\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/gauss.png}}
\caption{Gaussian Distribution Model}
\label{fig:Gaussian}
\end{figure}

The frequency distribution for different candidate strings is detected better where the original true values' countings are larger. We can see from the analysis results that, the variation distance, which is 0.149, is a little bit better than the uniform distribution. This can be explained by the fact that RAPPOR tends to detect those strings that have more counts/higher frequency. In Gaussian distribution, the strings that are close to 0 or 100 tends to have smaller counts, and thus they are not detected by RAPPOR. This can be observed easily from Figure \ref{fig:Gaussian}. Since those strings that are not detected are sampled with low counts in the theoretical Gaussian distribution, those unmathches do not contribute as much to the variation distance as in an uniform distribution.
In terms of the detection rate and false positive rate, since there are a lot of low frequency values undetected so these two values aren't really good, 67/100 and 10.0\%(10), respectively. But if the real scenario is to do statistics on the most frequent values, then the low detection rate in Gaussian distribution will not be affected much. 

Since Gaussian distribution is the most frequently used statistical model, we will also use it in the parameter tuning experiments. 


\subsubsection{Poisson Distribution}

Poisson distribution is a discrete probability distribution that is used to show the probability of a given number of events that usually occur in a fixed interval of time and  some space if these events occur with a known average rate and should be independent of the time since the last event. One specific example is the number of phone calls received by a call center per hour. The fitting result is shown in \ref{fig:poi}. In the Poisson test, we set the range to be 1 to 74 and Lambda to be 1. 
\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/poi.png}}
\caption{Poisson Distribution Model}
\label{fig:poi}
\end{figure}

In the statistical result, Poisson distribution has distance variance 0.125 and false positive rate 14.0\%(14) as well as detection rate 45/71. It is better compared with uniform distribution.

\subsubsection{Log-normal Distribution}

When the data is not symmetry-like and skews toward positive side, one common model to describe it is lognormal distribution. It is usually characterized by range and mean deviation of the distribution on the log scale parameter. In our model, the range is 1 to 100 and mean on the log scale is 3. Result is shown in figure \ref{fig:log}. From the statistical result, the total variation distance is 0.159, detection rate is 71/100 and false positive rate is 11.0\% (11). From the detection rate we can see that due to the theoretical distribution of Log-normal, quite some strings have low counts/frequencies, therefore they are not easily detected, which results in a relatively low detection rate.


\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/log.png}}
\caption{Log-normal Distribution Model}
\label{fig:log}
\end{figure}


\subsubsection{Gamma Distribution}

Gamma and Weibull distributions are two distributions that are thought to be closely related to the lognormal distribution. The common exponential distribution and chisquared distribution are two special cases of the gamma distribution. Gamma distribution is also the maximum entropy probability distribution for a random variable X for which E[X] is fixed and greater than zero. In our Gamma distribution experiment, we set shape and scale both to be 2.0 and the result is shown in figure \ref{fig:gamma}. The detection rate is still not very good (57/100) and false positive rate and total variation distance is 0.152. And the false positive rate is 11.0\%(11).

\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/gamma.png}}
\caption{Gamma Distribution Model}
\label{fig:gamma}
\end{figure}

\subsubsection{Weibull Distribution}

As we mentioned above, Weibull distribution is thought to be closely related to the lognormal distribution. For the lognormal distrbution, if we change parameters to let skewness become more severe, then it's gradually shifted to the Weibull distribution. In our Weibull distribution experiment, we set shape to be 0.5 and scale to be 1 and the result is shown in figure \ref{fig:gamma}. The detection rate is still not very good (5/14) and false positive rate and total variation distance is 0.0\% (0) (It may due to the less range and indicates that Weibull distribution is not that sensitive when applied RAPPOR) and 0.016.

\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/wei.png}}
\caption{Weibull Distribution Model}
\label{fig:wei}
\end{figure}


\subsubsection{Exponential Distribution}

Exponential distribution is the distribution that has been discussed in the original RAPPOR paper \cite{erlingsson2014rappor}. It is usually used to describe the time between events in a PoissonI process and it is a particular case of gamma distribution. The fitting result is shown in \ref{fig:exp}. In our test, we set the range to be 1 to 100 and rate to be 0.05. The variation distance is 0.138 and detection rate \& false positive rate is 67/100 \& 5.0\%(5). Since exponential distribution is the extreme case of poisson, so we usually will apply Poisson distribution to the case we count.

\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/exp.png}}
\caption{Exponential Distribution Model}
\label{fig:exp}
\end{figure}

\subsubsection{Chi-squared Distribution}

Chi-squared distribution is the distribution of a sum of the squares of  k independent standard normal random variables and it is also the special case of gamma distribution. The specific case may be in hypothesis testing or in construction of condidence intervals. If we set the degree of freedom to be 6, then the fitting result is like what is shown in \ref{fig:poi}. 
\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/chisquare.png}}
\caption{Chi-squared Distribution Model}
\label{fig:chisquare}
\end{figure}

In the statistical result, Chi-squared distribution has distance variance 0.059 and false positive rate 6.0\%(6) as well as detection rate 21/34. Especially the total variation distance result is better compared with uniform distribution and other distributions.

\subsubsection{Student's t-Distribution}

The student's t-distribution arises when estimating the mean of a normally distributed population and the sample size should be small and the population standard deviation is unknown. So in this case, the sample size of this distribution should be set to be small but we still want to try to use RAPPOR and see what the fitting result looks like. It is shown in \ref{fig:poi}. 
\begin{figure}[h]
\centerline{\includegraphics[width=11cm]{dist/student_dist.png}}
\caption{Student's t Distribution Model}
\label{fig:student_dist}
\end{figure}

If we set the degree of freedom to be 2, then in the statistical result, student's t-distribution has distance variance 0.075 and false positive rate 5.0\% (5). But from the detection rate (24/100), we can find out that we really cannot apply student's t-distribution to RAPPOR algorithm.

\subsubsection{Inverse Proportional Function Fitting}

We can even freely define functions, and show how the function fits by using RAPPOR algorithm and observe the influence of parameters during fitting processes. Here we set function to be f(x)=b/x (b is constant), and we separately set b to be 1 and 1.5. The fitting result is shown in \ref{fig:zipf}. In the Poisson test. The statistical results indicate that f(x)=1/x has better detection rate but f(x)=1.5/x has better false positive rate and total variation distance. (detailed result can be found in the following table)

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{dist/zipf1.png}
        \caption{function f(x)=1/x}
        \label{fig:zipf1}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
    %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{dist/zipf1_5.png}
        \caption{function f(x)=1.5/x}
        \label{fig:zipf1_5}
    \end{subfigure}
    \caption{Inverse Proportional Function Fitting}\label{fig:zipf}
\end{figure}



\subsection{Comparison of Different Distribution Model}
Here we use a table to show the statistical fitting results of all the distributions and functions we discussed:

\begin{table}[h]
\centering
\small
\caption{Fitting Result Comparison For Different Distribution Models and Functions}
\vspace{0.1cm}
\label{params}
\begin{tabular}{|c|c|c|c|}
\hline
\textbf{Statistical Standard}  & \textbf{r/a} & \textbf{fp} & \textbf{tv} \\ 
\textbf{}  & \textbf{(detection rate)} & \textbf{(false positive rate)} & \textbf{(total variation distance)} \\ \hline
\textbf{Uniform Distribution} & 97/100  & 8.0\% (8)  & 0.160 \\ \hline
\textbf{Gaussian Distribution} & 67/100  & 10.0\% (10)  & 0.149 \\ \hline
\textbf{Poisson Distribution} & 45/71  & 14.0\% (14)  & 0.125 \\ \hline
\textbf{Lognormal Distribution} & 71/100  & 11.0\% (11)  & 0.159 \\ \hline
\textbf{Gamma Distribution} & 57/100  & 11.0\% (11)  & 0.152 \\ \hline
\textbf{Weibull Distribution} & 5/14  & 0.0\% (0)  & 0.016 \\ \hline
\textbf{Exponential Distribution} & 67/100  & 5.0\% (5)  & 0.138 \\ \hline
\textbf{Chi-squared Distribution} & 21/34  & 6.0\% (6)  & 0.059 \\ \hline
\textbf{Student's t-Distribution} & 24/100  & 5.0\% (5)  & 0.075 \\ \hline
\textbf{Function f(x)=1/x} & 63/100  & 8.0\% (8)  & 0.143 \\ \hline
\textbf{Function f(x)=1.5/x} & 25/100  & 6.0\% (6)  & 0.100 \\ \hline
\end{tabular}
\end{table}

From this table we can see that different distribution models or functions perform better or worse considering different statistical standards. If we want more candidate true values to be detected after using RAPPOR to count frequency distributions, the simulated or practical model should approach the models which has relatively small counting variations between different candidates, like uniform distribution and lognormal distribution. When considering the false positive rate, which means we want to have counts significantly different from 0 as small as possible, we may want some distributions like exponential distribution. However, if we want the general statistical result to be similar to the original true value set as much as possible, we may want some distribution like Weibull distribution and Chi-squared distribution. However, some commonly-used distribution models cannot be prevented from using, like Gaussian distribution. What's more, there are some distribution models that cannot use RAPPOR because of the premises they set. Like Student's t-distribution, it is said they should have smaller sample size, which is against the premises of RAPPOR to have enough sample size to guarantee effectiveness of statistical results.



