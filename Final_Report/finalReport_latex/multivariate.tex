
\section{Multivariate Distribution}
\label{multivariate}
\subsection{Multivariate query \& Joint distribution estimation}
\subsubsection{Goal}
One of the assumptions of RAPPOR is that we only analyze the distribution of 
one single variable. However, for many times, we want to estimate joint distribution of two or more variables. 
The idea of RAPPOR can be used and adapted to learn the associations and correlations among multiple variables for multiple properties, which are all collected in a privacy-preserving manner. Most of the discussions in this section is based on paper \cite{fanti2016building}.

\subsubsection{Method used}
\begin{itemize}
  \item Bayes' theorem:
  \begin{itemize}
  	\item Aim: to calculate posterior probability: ${P(C_k|x)}$ from ${P(C_k)}$, ${P(x)}$ and ${P(x|C_k)}$.
	\begin{itemize}
  		\item ${P(C_k|x)}$ is the posterior probability of the class ${C_k}$ given predictor vector ${x}$;
		\item ${P(C_k)}$ is the prior probability of class ;
		\item ${P(x|C_k)}$ is the likelihood which is the probability of predictor's given class;
 		\item ${P(x)}$ is the prior probability of predictor.
	\end{itemize}
  	\item Formally, given a program instance to be classified, represented by a vector , representing some  features (independent variables), it assigns to this instance probabilities:
$$ P(C_k|x_1, \; .., \; x_n) = P(C_k|x) $$
For each of  possible outcomes or class , and it could be calculated by:
$$ P(C_k|x) = \frac{P(x|C_k)P(C_k)}{P(x)} $$
  \end{itemize}
  \item EM (Expectation Maximization) algorithm:
  \begin{itemize}
  	\item It is an iterative method to find maximum likelihood estimates.
  	\item Implementation step: 
	\begin{itemize}
 		\item Expectation (E) step: create a function for the expectation of the log-likelihood evaluated using the current estimate for the parameters.
  		\item Maximization (M) step: computes parameters maximizing the expectation.
	\end{itemize}
  \end{itemize}
\end{itemize}

\subsubsection{Input \& Output}
\begin{itemize}
  \item Input: two random variables ${X}$ and ${Y}$ (or more than two, however, in our analysis, we only discuss the joint distribution of two variables. It will be similar if the number of joint distribution is more than two), both collected using Basic RAPPOR.
  \item Output: Assuming ${x_i}$ and ${y_j}$ are the candidate strings from ${X}$ and ${Y}$, respectively, using the conditional probability of true values ${X}$ and ${Y}$ given the noisy representations ${X'}$ and ${Y'}$ derived from RAPPOR, which is ${P(X=x_i, \; Y=y_j|X',\; Y')}$, we will finally output the joint distribution of ${X}$ and ${Y}$: for each combinations of categories  ${i}$ and ${j}$.
  \item To sum up, we have the input \& output RAPPOR form: 
  $$ X,\; Y\; \to P(X=x_i,\; Y=y_j|X', \; Y') \; \to p_{ij}=P(X|x_i,\; Y=y_j).$$
\end{itemize}

\subsubsection{Parameters}
\begin{itemize}
  \item ${m}$ and ${n}$: number of categories in ${X}$ and ${Y}$, respectively.
  \item ${p_{ij}}$: true joint distributions of ${X}$ and ${Y}$.
  \item ${k}$: size of Bloom Filter ${B}$.
  \item ${N}$: pairs which are collected from ${N}$ distinct (independent) clients to calculate the noisy representation of ${X}$ and ${Y}$ created by RAPPOR.
  \item ${X_i}$ and ${Y_j}$: the events that ${X=x_i}$ and ${Y=y_j}$.
  \item ${p, q}$: one of two tunable randomized response parameters.
  %\item ${q}$: one of two tunable randomized response parameters.
\end{itemize}

\subsubsection{Estimating joint distributions with the EM algorithm}
\begin{itemize}
  \item Calculate the noisy representation of ${X}$ and ${Y}$ created by RAPPOR:  ${X' = RAPPOR(X)}$,  ${Y'=RAPPOR(Y)}$.
  \item Calculate the probability ${P(X'|X=x_1)}$ (and then ${P(X'|X=x_2)}$, ${P(X'|X=x_3)}$ ..., ${P(X'|X=x_m)}$ and ${P(Y'|Y=y_i)}$ respectively), here ${X'}$ represents the possible Bloom Filter representation of ${x_1}$. Since it is in Basic RAPPOR, ${x_i}$'s Bloom Filter representation has a one in the first position and zeros elsewhere, so there is the related possibility:
  $$ P(X'|X=x_1) = q^{x_1'} (1-q)^{1-x_1'} \times p^{x_2'} (1-p)^{1-x_2'} $$
  		$$\times \; ... \; \times p^{x_i'} (1-p)^{1-x_i'}$$
		$$\times \; ... \; \times p^{x_m'} (1-p)^{1-x_m'}$$
		Other probabilities ${P(X'|X=x_2)}$, ${P(X'|X=x_3)}$ ..., ${P(X'|X=x_m)}$ and ${P(Y'|Y=y_i)}$ calculating methods are similar.
  \item Derive the conditional probability of true values  ${X}$ and ${Y}$ using the consequence of Bayes' theorem:
  \begin{eqnarray*}
			P(X=x_i, \; Y=y_j|X', \; Y_j)  = \frac{p_{ij}P(X', \; Y'|X_i, \; Y_j)} {\sum_{k=1}^m {\sum_{l=1}^n {p_{kl}P(X', \; Y'|X_k, \; Y_l)}}} \\
			=  \frac{p_{ij}P(X'|X_i)P(Y'|Y_j)} {\sum_{k=1}^m {\sum_{l=1}^n {p_{kl}P(X'|X_k)P(Y'|Y_l)}}}
		\end{eqnarray*}
  \begin{itemize}
 	\item ${P(X', \; Y'|X, \; Y)}$ is the joint probability of the two noisy outcomes given both the related true value. Since ${X'}$ and ${Y'}$ are conditionally independent given both ${X, \; Y}$, ${P(X', \; Y'|X, \; Y) = P(X'|X)P(Y'|Y)}$, which is easy to precisely describe.
	\end{itemize}
	\item Proceed EM algorithm \cite{dempster1977maximum}:
	\begin{itemize}
  		\item \textbf{Initialize}: ${\hat{p_{ij}^{0}} = \frac{1}{nm}, \; 1 \le i \le m, \; 1 \le j \le n \;}$ (we assume it to be a uniform distribution) 
  		\item \textbf{Update} ${\hat{p_{ij}^t}} $ using 
		\begin{eqnarray*}
		\hat{p_{ij}^{t+1}} = P(X=x_i, \; Y=y_j)\\
			=\frac{1}{N} \sum_{d=1}^N {P(X=x_i, \; Y=y_j|X_d', \; Y_d')} \\
			=\frac{1}{N} \sum_{d=1}^N {\frac{p_{ij}P(X_d', \; Y_d'|x_i, \; y_j)}{\sum_{k=1}^m {\sum_{l=1}^n {p_{kl}P(X_d', \; Y_d'|X_k, \; Y_l)}}} } \\
			=\frac{1}{N} \sum_{d=1}^N {\frac{p_{ij}P(X_d'|X_i)P(Y_d'|y_j)}{\sum_{k=1}^m {\sum_{l=1}^n {p_{kl}P(X_d'|X_i)P(Y_d'|y_j)}}} } \\
			=\frac{1}{N} \sum_{d=1}^N {\frac{\hat{p_{ij}^t}P(X_d'|X_i)P(Y_d'|y_j)}{\sum_{k=1}^m {\sum_{l=1}^n {\hat{p_{kl}^t}P(X_d'|X_i)P(Y_d'|y_j)}}} } 		
		\end{eqnarray*}
		\begin{itemize}
  			\item ${P(X=x_i, \; Y=y_j|X_d', \; Y_d')}$  is computed using the current estimates $\hat{p_{ij}^t}$ and the separate elements in the final analyzed equation can be derived respectively.
  		\end{itemize}
		\item \textbf{Repeat Update} step until reaching the final convergence:
		$$ max_{ij}|\hat{p_{ij}^{t+1}} - \hat{p_{ij}^t}| < \delta ^ *$$
		\begin{itemize}
 			 \item ${\delta ^ *}$ is the small positive value we defined to describe the accuracy of the joint distribution.
  			\item Finally we will get the maximum likelihood estimates of ${p_{ij}}$, which is the probability of ${x_i}$ and ${y_j}$'s joint distribution: ${p_{ij}=P(X|x_i,\; Y=y_j)}$ for each combinations of categories ${i}$ and ${j}$.
		\end{itemize}
	\end{itemize}
\end{itemize}

\subsection{Parallel Bloom Filters for Multiattribute Representation}
When estimating many attributes of one variable (query) and construct relations for more variables (queries), we use counting bloom filter to hash raw data for RAPPOR algorithm which is shown in Chapter \ref{counting_bf}. 

For the multi-variable situation, if we still consider using Standard Bloom Filter (SBF), the problem is that the overhead for the concatenation of different attributes or variables may delay query replies to users under the circumstance that multiple attributes have different formats like string and digit, etc. What's more, To get the hashed result for a single but long attribute array may cost much longer time. Think about the multiattribute representation in the SBF. Each attribute still need their own bloom filters in order not to mix up the different attributes, so the bloom filter array will be much longer than the single attribute bloom filter. 

\subsubsection{Parallel Bloom Filter structure}

In order to process different attributes at the same time, we consider the structure called Parallel Bloom Filter (PBF) \cite{xiao2010using}. In order to implement the operations on multiattribute items, we use this kind of PBF structure which contains Counting Bloom Filter (CBF) to support deletion of single data.

\paragraph{Parallel Bloom Filter} PBF is made up of ${p}$ parallel submatrics so that it can represent ${p}$ attributes. A submatrix consists of ${q}$ parallel arrays and can be used to represent ${q}$ hash functions for a single attribute. One array is composed of ${m}$ counters and is corresponding to one hash function. We use the segment form of the bloom filter for each attribute and it has the same filter size with the normal flat form of bloom filter, see Figure \ref{fig:SBF} \cite{xiao2010using} .
\begin{figure}[t]
\centerline{\includegraphics[width=11cm]{SBF.png}}
\caption{Bloom filter structures. (a) Flat form. (b) Segment form}
\label{fig:SBF}
\end{figure}


Therefore, an entry ${C_{[i][j][k]}}$ represents the ${k}$th counter, and in the ${j_{th}}$ array of the ${i_{th}}$ submatrix (${1 \leq k \leq m, 1 \leq i \leq p, 1 \leq j \leq q}$). Similarly, $k = {H_{[i][j]}(a_i)}$ represents the hash value computed by the ${j_{th}}$ hash function of the ${i_{th}}$ attribute of item ${a}$. So PBF needs  ${p \times q \times m}$ counters to store items of ${p}$ attributes.

However, using one single PBF to represent the ${p}$ attributes of an item will cause false positives because the basic PBF cannot distinguish one's attributes from the other's. Like if we have one raw data (Large, Red) and one raw data (Small, Green) for the plates which has exactly two attributes, then the (Large, Green) will be treated to be true for the existed dataset and we will answer yes if someone ask ${'Does\; there\; exist\; a\; plate\; that\; is\; large\; and\; has\; green\; color?'}$ And this kind of false positive is what we don't want.

\paragraph{Parallel Bloom Filter with Hash Table} Therefore, we add a Hash Table (HT) to the PBF and derive PBF-HT. Hash table is responsible for storing verification values of items which refers to each answer set from user corresponding to a set of queries. The verification value can verify ${p}$-attribute integrity from one item, like, $v_i = F(H_{[i][j]}(a_i))$, where ${F}$ is the verification function used by hash table and this equation represents the verification value of the ${i}$th attribute of item ${a}$. Similarly, the verification value for the whole item ${a}$ can be: ${V_a = \sum_{i=1}^{p} v_i}.$ It will be inserted into the hash table for incoming dependency test of variables with multi-attributes.

The structure of PBF-HT is shown in Figure \ref{fig:PBF} \cite{xiao2010using}. Using PBF-HT for this multiattribute query, we need to check if ${(1)}$ All of the ${p}$ attributes exist in the PBF structure and ${(2)}$ The sum of hash values for all ${p}$ attributes ${V_a}$ is in the HT. Only both of the conditions ${(1)}$ and ${(2)}$ holds, will we say that a particular item ${a}$ is one of the existing members in the PBF-HT. 
\begin{figure}[t]
\centerline{\includegraphics[width=11cm]{PBF_HT.png}}
\caption{PBF-HT structure using counting Bloom filters}
\label{fig:PBF}
\end{figure}

When it comes to the RAPPOR implementation, for the multi-attribute queries, we can directly use the same structure for the attributes of an item and put them all into one PBF-HT structure. Since the modified encoding RAPPOR algorithm and corresponding proof has been shown in Chapter \ref{counting_bf}, we will use the modified encoding and decoding RAPPOR algorithm aimed at counting bloom filter combined with the PBF-HT structure to achieve multivariate query and counting. So now, following the previous example, now we can answer the question like ${'How\; many\; plates\; that\; are\; both\; large\; and\; has\; green\; color?'}$.

\subsubsection{False positive rate and memory space comparison with Standard Bloom Filter}
In terms of false positive rate and memory space, the PBF-HT structure is proven to have less probability of false positive and need less space compared with SBF and simple PBF. We have proved in the Chapter \ref{hash_collision} that the probability of false positive of SBF is:
$$ f_{SBF} \simeq (1-e^{-\frac{qn}{M}})^q = (1-e^{-\frac{n}{m}})^q, $$
%1-e^{(-\frac{qn}{M})}^q = 1-e^{(-\frac{n}{m})}^q,$$
where the bloom filter has totally ${M}$ bits and ${q}$ hash functions to store ${n}$ items and the minimized value of it is ${1/2}^q = (0.6185)^q$, where ${q = (M/n) ln 2}$ and the probability of PBF is proven to be exponential with parameter ${q}$ too.
The probability of PBF-HT is proven to be \cite{xiao2010using}:
$$ 2 \Phi (\frac{\sqrt{3} n}{(m-1)\sqrt{pq}}) - 1,$$
where ${\Phi(\star)}$ represents the standard Normal Distribution which is illustrated in Chapter \ref{distributions}.
For the space, we could find hints from Chapter \ref{counting_bf} that counting bloom filter will save space in the log level of SBF. And since PBF-HT has better false positive probability than SBF and PBF, in order to reach the same false positive probability to keep certain dataset differential privacy and storing, PBF-HT will require less storage space compared with SBF and PBF.




%The hardware verification is done at design time, following the process depicted in Figure~\ref{fig:process}.


%\subsection{Components of SecChisel}

%SecChisel is based on a modified {\bf Chisel language and tools}.


% verifying whole design in a brute force fashion would take more than $10^{13}$ years, 

%\begin{enumerate}
%\item Designer defines a design in SecChisel (Chisel with addition of new security labels).
%\item SecChisel generates an intermediate level circuit defined in FIRRTL (Flexible Intermediate 
%\end{enumerate}





