\section{Introduction and Motivation}
\label{introduction}

In order to help improve services or products, it is getting more and more commonplace for organizers to analyze user data. However, in many cases, these user data contain users' sensitive information and therefore inappropriate data collection or data analysis would bring information leakages to users. How to collect and analyze user data in a privacy-preserving way while at the same making sure that reasonable data analysis is possible is a challenging topic nowadays. 


One conventional way to realize privacy-preserving data analysis is by outsourcing the user data to a trusted third party, and then this third party is responsible for analyzing the provided user data and finally replies back the organizers' queries, as shown in figure \ref{fig:third}. However, such a model itself has many potential attacks. For example, there is chance that the third party itself gets corrupted, in this case, all the user data's privacy is compromised.  There is also chance that a man-in-the-middle attacker between the plaintext user data source and the third party would intercept or maliciously modify the user data since they are in the plaintext format.



\subsection{Differential privacy}
In order to get rid of such a third party and make sure that each individual's data is private, we need a protocol that ensures local privacy. A common method for collecting user data's statistical information without access to individual-level data points is based on randomized response, which is a technique that satisfies a privacy guarantee known as local differential privacy \cite{dwork2008differential}. 
%The idea of randmized response will be introduced in section \ref{Randomized response}.

\begin{figure}
  \includegraphics[width=\linewidth]{third_party}
  \caption{Digram for data outsourcing model}
  \label{fig:third}
\end{figure}

The differential privacy setting is described as follows:
These parameters are defined: 
\begin{itemize}
  \item ${\chi}$: the data universe.
  \item ${x}$: raw data from dataset.
  \item ${n}$: number of individuals / users.
  \item ${M}$: randomized mechanism.
  \item ${Q}$: query space.
  \item ${y}$: output space of ${M}$.
\end{itemize}

Consider a trusted curator holds a dataset  ${x}$ (raw data) about ${n}$ individuals, which we could model it as a tuple ${x\in \chi ^n}$, where ${\chi}$ represents the data universe. There is an interface as the outer layer of data, which is a randomized mechanism ${M: x^n \times Q \to y}$. In order not to introduce the continuous probability formalism, we will assume that ${\chi ,\; Q, \; y }$ are all discrete variables.

Then for a dataset ${x = (x_1, \; x_2, \; ..., \; x_n)}$ with single query , we will have:

\begin{figure}[h]
\centerline{\includegraphics[width=12cm]{df.png}}
\caption{Diagram for the Differential Privacy Model, taken from \cite{vadhan2016complexity}}
\label{fig:df}
\end{figure}

A simplified definition of differential privacy can be given in this way, which basically says that the modification of one single user's data point will not bring observable effect to the final data distribution result. Suppose we have a randomized algorithm A which satisfies ${\epsilon}$-differential privacy, then if we have dataset ${x}$ and ${x'}$ which only differs in one user's data point, we will have:
$$P(A(x)\in R) \le e^\epsilon P(A(x')\in R), \; for \; all \; R \subseteq Range(A)$$

 

\subsection{Randomized response}
RAPPOR builds on the idea of randomized response \cite{warner1965randomized}, a surveying technique developed for sensitive data collections. A good example to describe this technique invloves a question on a sensitive topic, such as "Are you one of LBGT?". Respondents who need to answer this question is asked to first flip a fair coin, in secret, and answers his/her true answer if the flip result is HEAD. Otherwise, he/she is asked to flip another fair coin, and if the result of the second flip is HEAD, answer "YES", otherwise, answer "NO". Such a randmized technique helps protect respondents' privacy, since they have strong deniability for both "YES" and "NO" answers. 

Apart from privacy promise, such techniques also enable efficient computation. It is easy to see that, if Y is the portion of "YES" among all the collected reports, the portion of "YES" in the true answers is 2(Y-0.25).

Importantly, for one-time collection, the above randmomized survey mechanism will protect the privacy of any specific respondent.


\subsection{Assumption for RAPPOR}
The privacy-preserving data-collection mechanism that we are going to analyze and extend is called RAPPOR, which makes use of randomized response to guarantee local differential privacy for report of every individual. 

RAPPOR is motivated by the application domain that cloud server operators need to collect up-to-date statistics about the activity of their users and their client-side-software. In most cases, the goal of data collection using RAPPOR is to learn which strings are present in the sampled population and what their corresponding frequencies are.

Before talking about the RAPPOR algorithm, it is important to first know the two assumptions of RAPPOR:
\begin{itemize}
  \item Assumption 1: Organizers/Aggregators only needs to learn the distribution of a single variable, in isolation.
  \item Assumption 2: Organizers/Aggregators know the data dictionary of possible string values in advance. 
\end{itemize}
All of our following discussions except the extension are based on these two assumptions.

RAPPOR provide strong privacy guarantees combined with high utility where privacy and utility are two important properties we want to optimize and balance in the extended work. What's more, RAPPOR collects statistics from end-user, client-side software and permits collecting over large number of clients on client-side values, strings, etc. It performs locally on the client side and doesn't require a trusted third party. For the application of RAPPOR, it can be used to collect statistics on categorical client properties, having each bit in a client's response represent whether or not that client belongs to a category. Secondly, it can be used to gather population statistics on numerical and ordinal values, which could improve collecting and utilizing relevant information about shape of empirical distribution. 



\subsection{Paper organization}

The remainder of the work is organized as follows.
Section \ref{encoding} gives the detailed explanation of how to construct the RAPPOR algorithm based on the Bloom Filter and randomization steps, which is the encoding step.
Section \ref{decoding} talks about how to remove the noise and calculate the frequency of each possible answer of a particular query, which is the decoding step.
%Section \ref{ideas} gives some intuitions about our application and extension work of RAPPOR.
Section \ref{distributions} shows how different distribution models fit RAPPOR.
Section \ref{params} talks about how we tune different security parameters and general parameters and we will show the influence of them on both accuracy and privacy in RAPPOR.
Section \ref{Counting_bf} introduces Counting Bloom filter and talks about how we extend RAPPOR algorithm to multi-queried version based on Counting Bloom filter.
Section \ref{multivariate} shows our understandings about multi-variate queries when using RAPPOR and introduce the technique we want to use for multi-variate queries, which is parallel Bloom filter, which is based on Counting Bloom filter as we talked in the Chapter \ref{Counting_bf}.
Section \ref{adaptive} shows some intuition about how to do adaptive queries by using RAPPOR.
Section \ref{hash_collision} shows how to calculate and limit hash collision when using Bloom filter.
Section \ref{conclusion} concludes the whole report paper.



