\select@language {english}
\contentsline {section}{Abstract}{2}
\contentsline {section}{\numberline {1}Introduction and Motivation}{2}
\contentsline {subsection}{\numberline {1.1}Differential privacy}{2}
\contentsline {subsection}{\numberline {1.2}Assumption for RAPPOR}{3}
\contentsline {subsection}{\numberline {1.3}Paper organization}{3}
\contentsline {section}{\numberline {2}RAPPOR Encoding Algorithm and Attack Model}{4}
\contentsline {subsection}{\numberline {2.1}Goal}{4}
\contentsline {subsection}{\numberline {2.2}Method used}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Randomized response}{4}
\contentsline {subsubsection}{\numberline {2.2.2}Local differential privacy}{4}
\contentsline {subsubsection}{\numberline {2.2.3}Bloom filters}{4}
\contentsline {subsubsection}{\numberline {2.2.4}Input \& Output}{5}
\contentsline {subsubsection}{\numberline {2.2.5}Parameters}{5}
\contentsline {subsubsection}{\numberline {2.2.6}RAPPOR algorithm}{5}
\contentsline {subsubsection}{\numberline {2.2.7}Visualized RAPPOR encoding model diagram}{7}
\contentsline {subsubsection}{\numberline {2.2.8}Different kinds of RAPPOR modifications}{7}
\contentsline {section}{\numberline {3}Decoding}{7}
\contentsline {subsection}{\numberline {3.1}Operation steps}{8}
\contentsline {section}{\numberline {4}Ideas}{9}
\contentsline {section}{\numberline {5}Experiment and Evaluation of Different Distributions}{9}
\contentsline {subsection}{\numberline {5.1}General parameters and judgements used}{9}
\contentsline {subsection}{\numberline {5.2}Different Distribution Models}{10}
\contentsline {subsubsection}{\numberline {5.2.1}Uniform Distribution}{10}
\contentsline {subsubsection}{\numberline {5.2.2}Gauss Distribution}{11}
\contentsline {subsubsection}{\numberline {5.2.3}Poisson Distribution}{11}
\contentsline {subsubsection}{\numberline {5.2.4}Log-normal Distribution}{12}
\contentsline {subsubsection}{\numberline {5.2.5}Gamma Distribution}{12}
\contentsline {subsubsection}{\numberline {5.2.6}Weibull Distribution}{13}
\contentsline {subsubsection}{\numberline {5.2.7}Exponential Distribution}{13}
\contentsline {subsubsection}{\numberline {5.2.8}Chi-squared Distribution}{13}
\contentsline {subsubsection}{\numberline {5.2.9}Student's t-Distribution}{14}
\contentsline {subsubsection}{\numberline {5.2.10}Inverse Proportional Function Fitting}{14}
\contentsline {subsection}{\numberline {5.3}Comparison of Different Distribution Model}{15}
\contentsline {section}{\numberline {6}Parameters Tuning}{16}
\contentsline {subsection}{\numberline {6.1}Impact of f}{16}
\contentsline {subsection}{\numberline {6.2}Impact of p and q}{18}
\contentsline {subsection}{\numberline {6.3}Impact of N: number of reports}{21}
\contentsline {section}{\numberline {7}Counting Bloom Filter}{21}
\contentsline {subsection}{\numberline {7.1}Multiple-time collection in Basic RAPPOR}{21}
\contentsline {subsection}{\numberline {7.2}Extension: Basic RAPPOR with counting Bloom filter}{24}
\contentsline {paragraph}{Counting Bloom filter}{24}
\contentsline {paragraph}{Basic RAPPOR with counting Bloom filter}{25}
\contentsline {subsection}{\numberline {7.3}Differential privacy of extended Basic RAPPOR}{25}
\contentsline {paragraph}{Assumptions}{25}
\contentsline {paragraph}{Differential privacy of the permanent randomized response}{25}
\contentsline {paragraph}{Decoding of extended Basic RAPPOR}{27}
\contentsline {paragraph}{Server space saving}{29}
\contentsline {section}{\numberline {8}Hash Collision}{29}
\contentsline {subsection}{\numberline {8.1}Hash collision for standard bloom filter}{29}
\contentsline {subsection}{\numberline {8.2}Hash collision for counting bloom filter}{30}
\contentsline {section}{\numberline {9}Multivariate Distribution}{31}
\contentsline {subsection}{\numberline {9.1}Multivariate query \& Joint distribution estimation}{31}
\contentsline {subsubsection}{\numberline {9.1.1}Goal}{31}
\contentsline {subsubsection}{\numberline {9.1.2}Method used}{31}
\contentsline {subsubsection}{\numberline {9.1.3}Input \& Output}{32}
\contentsline {subsubsection}{\numberline {9.1.4}Parameters}{32}
\contentsline {subsubsection}{\numberline {9.1.5}Estimating joint distributions with the EM algorithm}{32}
\contentsline {subsection}{\numberline {9.2}Parallel Bloom Filters for Multiattribute Representation}{34}
\contentsline {subsubsection}{\numberline {9.2.1}Parallel Bloom Filter structure}{34}
\contentsline {paragraph}{Parallel Bloom Filter}{34}
\contentsline {paragraph}{Parallel Bloom Filter with Hash Table}{35}
\contentsline {subsubsection}{\numberline {9.2.2}False positive rate and memory space comparison with Standard Bloom Filter}{36}
\contentsline {section}{\numberline {10}Adaptive Data Collection}{36}
\contentsline {subsection}{\numberline {10.1}Sample-size decreases as adaptive queries go}{38}
\contentsline {subsection}{\numberline {10.2}Sample-size stays the same as adaptive queries go}{40}
\contentsline {subsection}{\numberline {10.3}Summarization}{41}
\contentsline {section}{\numberline {11}Conclusion}{41}
\contentsline {section}{Acknowledgement}{41}
