Anonymization in Big Data:

k-anonymity:
https://epic.org/privacy/reidentification/Sweeney_Article.pdf
extensions and models..

differential privacy:
https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/dwork.pdf
http://link.springer.com/chapter/10.1007/978-3-540-79228-4_1
extensions and models..

Pros and Cons of these two general methods:
http://www.tdp.cat/issues11/tdp.a124a13.pdf

Tradeoff between scalability and privacy:
http://ieeexplore.ieee.org/document/6906834/
http://ieeexplore.ieee.org/document/6680880/?tp=&arnumber=6680880
http://ieeexplore.ieee.org/document/6911981/

Current problems in existing proposals/applications:
http://file.scirp.org/pdf/JIS_2013042311170360.pdf
https://arxiv.org/pdf/cs/0610105.pdf
https://www.cs.cornell.edu/~shmat/shmat_oak08netflix.pdf
https://www.rand.org/content/dam/rand/pubs/working_papers/WR1100/WR1161/RAND_WR1161.pdf


encryption related to big data:

Security in big data:
A survey on Security and Privacy Issues in Big Data
http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7412089
Securing the Big Data Life Cycle
http://files.technologyreview.com/whitepapers/Oracle-Securing-the-Big-Data-Life-Cycle.pdf
Securing Your Big Data Environment
https://www.blackhat.com/docs/us-15/materials/us-15-Gaddam-Securing-Your-Big-Data-Environment-wp.pdf
A Survey of Cryptographic Approaches to Securing Big-Data Analytics in the Cloud
http://www.ieee-hpec.org/2014/CD/index_htm_files/FinalPapers/28.pdf
Top Ten Big Data Security and Privacy Challenges
http://www.isaca.org/groups/professional-english/big-data/groupdocuments/big_data_top_ten_v1.pdf

Encrypted search in big data:
Encrypted search
https://cs.brown.edu/~seny/pubs/esearch.pdf
Encrypted Keyword Search in a Distributed Storage System
http://people.csail.mit.edu/akiezun/encrypted-search-report.pdf
Encrypted Search
http://cra.org/ccc/wp-content/uploads/sites/2/2016/05/Seny-Kamara-Symposium-slides.pdf

Public key encryption in big data:
Searchable Public-Key Encryption with Data Sharing in Dynamic Groups for Mobile Cloud Storage
http://www.jucs.org/jucs_21_3/searchable_public_key_encryption/jucs_21_03_0440_0453_xia.pdf 

Data encryption strategy of big data (D2ES model):
Privacy-Aware Adaptive Data Encryption Strategy of Big Data in Cloud Computing
http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7545931


These are just sample files.

General idea is:
First know the two basic algorithms for big data anonymization, then know their applications and pros/cons.
Finally look at the current attacks and try to propose an anonymization algorithm which scales better while 
preserves enough security.



